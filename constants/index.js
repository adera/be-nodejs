module.exports = {
    ROLE_ADMIN: 'adm',
    ROLE_PEGAWAI: 'peg',
    QUERY_STATUS: `case when status = 0 then 'Menunggu Approval' when status = 1 then 'Sudah di Approve' when status = -1 then 'Ditolak' else 'unknown' end as status`
}