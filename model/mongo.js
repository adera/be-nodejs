const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const FcmToken = new Schema({
    user_id: {type: Number, index: true},
    token: {type: Array, index: true}
});

const Notifications = new Schema({
    to: {type: Number, index: true},
    title: {type: String},
    body: {type: String},
    type: {type: String},
    is_read: {type: Boolean}
});

module.exports = {
    FcmToken: mongoose.model('FcmToken', FcmToken),
    Notifications: mongoose.model('Notifications', Notifications)
};