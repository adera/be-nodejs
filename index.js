
const express = require('express');
const {errorHandler, checkToken} = require('./utils/middleware');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const moment = require('moment');
const serveIndex = require('serve-index');
const app = express();


const AuthController = require('./controllers/auth_controller');
const RoleController = require('./controllers/role_controller');
const RoleMenuController = require('./controllers/role_menu_controller');
const RegisterController = require('./controllers/register_controller');
const RefController = require('./controllers/ref_controller');
const DashboardController = require('./controllers/dashboard_controller');
const UserController = require('./controllers/user_controller');
const CompanyController = require('./controllers/company_controller');

const BisnisController = require('./controllers/bisnis_controller');
const LokasiBisnisController = require('./controllers/lokasi_bisnis_controller');
const TransaksiController = require('./controllers/transaksi_controller');


app.use(helmet());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/ftp', express.static('public'), serveIndex('public', {'icons': true}));


app.get('/', (req, res) => res.send('Hello World!'));
app.use((req, res, next) => {
    console.log("------------- Request");
    console.log("Method: "+ req.method);
    console.log("URI: "+ req.originalUrl);
    console.log("User-Agent: "+ req.headers['user-agent']);
    console.log("Remote Address "+ req.connection.remoteAddress);
    console.log("Datetime: "+ moment(new Date()).format('YYYY-MM-DD HH:mm:ss'));
    console.log("Payload: ", req.body);
    next();
});

app.use('/auth', AuthController);
app.use('/role',checkToken, RoleController);
app.use('/role-menu',checkToken, RoleMenuController);
app.use('/register',checkToken, RegisterController);
app.use('/ref', RefController);
app.use('/dashboard',checkToken, DashboardController);
app.use('/user',checkToken, UserController);
app.use('/company',checkToken, CompanyController);

app.use('/bisnis',checkToken, BisnisController);
app.use('/lokasibisnis',checkToken, LokasiBisnisController);
app.use('/transaksi',checkToken, TransaksiController);


app.use((err, req, res, next) => {
    errorHandler(err, res)
});



app.listen(8002, () => {
  console.log(`Tes run on port 8002`);
});


