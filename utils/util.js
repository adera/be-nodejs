const Ajv = require('ajv');
const ajv = new Ajv();
const {RequestValidationError} = require('../exceptions/business_exception');
const pad = require('pad');
const moment = require('moment');

const validateRequest = (request, schema) => {
    let valid = ajv.validate(schema, request);
    if(!valid)
        throw new RequestValidationError('Invalid request, please read API docs carefully!', 400, ajv.errors)
};

const sendResponseSuccess = (res, data, msg, statusCode) => {
    if (data != null)
        return res.json({
            'status': 'success',
            'statusCode': statusCode || 200,
            'message': msg || 'Success!',
            'data': data
        });

    return res.json({
        'status': 'success',
        'statusCode': statusCode || 200,
        'message': msg || 'Success!',
    });
};


const docNumberGenerator = async (seq, typedoc) => {
    try {
        let n = pad(4, seq, '0');
        return `RRJBS${moment().format('YYYYMMDD')}${typedoc}${n}`
    } catch (error) {
        throw error;
    }
};

const docNumberMAP = async (seq) => {
    try {
        let n = pad(3, seq, '0');
        return `${n}/MAPRP-KS/CNR/${moment().format('YYYY')}`
    } catch (error) {
        throw error;
    }
};

const docNumberSKP = async (seq) => {
    try {
        let n = pad(3, seq, '0');
        return `${n}/SKPRP-KS/CNR/${moment().format('YYYY')}`
    } catch (error) {
        throw error;
    }
};


module.exports = {
  validateRequest,
  sendResponseSuccess,
  docNumberGenerator,
  docNumberMAP,
  docNumberSKP
};