const {
    RequestValidationError,
    NotFoundError,
    ForbiddenError, UniqueError, FileError} = require('../exceptions/business_exception');
const jwt = require('jsonwebtoken');

const errorHandler = (err, res) => {
    console.error(err);
    if (err instanceof RequestValidationError){
        return res.status(err.status).json({
            status: 'error',
            statusCode: err.status,
            message: err.message,
            reason: err.reason
        })
    } else if (err instanceof NotFoundError){
        return res.status(err.status).json({
            status: 'error',
            statusCode: err.status,
            message: err.message
        })
    } else if (err instanceof ForbiddenError){
        return res.status(err.status).json({
            status: 'error',
            statusCode: err.status,
            message: err.message
        })
    }else if (err instanceof UniqueError){
        return res.status(err.status).json({
            status: 'error',
            statusCode: err.status,
            message: err.message
        })
    }else if (err instanceof FileError){
        return res.status(err.status).json({
            status: 'error',
            statusCode: err.status,
            message: err.message
        })
    }else{
        return res.status(500).json({
            status: 'error',
            statusCode: err.status,
            message: 'Internal server error!',
            reason: err
        })
    }
};

const checkToken = async (req, res, next) => {
    let token = req.headers['x-access-token'] || req.headers['authorization']; // Express headers are auto converted to lowercase
    if(token){
        token = token.slice(7, token.length);
        jwt.verify(token, `2BD988BF1570DE653C14BA90770F8E4A412DCC0B`, (error, decoded) => {
            if(error) {
                return res.status(401).json({
                    status: 'error',
                    statusCode: 401,
                    message: 'Invalid token!'
                });
            }else{
                req.user = decoded;
                next();
            }
        });
    }else{
        return res.status(401).json({
            status: 'error',
            statusCode: 401,
            message: 'Token is not supplied!'
        });
    }
};

const permit = (...allowed) => {

    const isAllowed = (roles) => {
        let c_minus = 0;
        roles.forEach(item => {
            let min = allowed.indexOf(item);
            if(min == -1){
                c_minus += 1;
            }
        });

        if(c_minus == roles.length)
            return false;

        return true;
    };

    return (req, res, next) => {
        if(isAllowed(req.user.roles)){
            next();
        }else{
            throw new ForbiddenError("You're not allowed use this operation!");
        }
    }
};

module.exports = {
    errorHandler,
    checkToken,
    permit
};