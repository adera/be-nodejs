const {knex} = require('../config/db');
const moment = require('moment');
const {NotFoundError, ForbiddenError, UniqueError} = require('../exceptions/business_exception');
const {QUERY_STATUS} = require('../constants/index');
const {docNumberGenerator} = require('../utils/util');
const bcrypt = require('bcryptjs');
const { Pool } = require('pg');
const rp = require('request-promise');

const list = async (user) => {
    try {
        console.log(user);
        const cek = await knex('role_menu')
        .join('menus','menus.id','=','role_menu.id_menu')
        .select('menus.*')
        .where('is_active',1)
        .where('id_role',user.role_id)
        .where('id_company',user.id_company);
        return cek;
    } catch (error) {
        throw error;
    }
};




module.exports = {
    list
};
