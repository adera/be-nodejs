const {knex} = require('../config/db');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const {NotFoundError, ForbiddenError} = require('../exceptions/business_exception');


const moment = require('moment');
const { Pool } = require('pg');
moment.locale('id');

const login = async (username, password, fcm_token) => {
    try{
        let user_exist = await knex('users').where({
            username: username
        }).select('id','password').first();

        if(typeof user_exist == 'undefined'){
            throw new NotFoundError("User doesn't exists!", 404);
        }else{
            const match = await bcrypt.compareSync(password, user_exist.password);
            if(match){
                const employee = await knex('users')
                .where({'id': user_exist.id}).first();
				
               
                let token = jwt.sign(
                    {
                        id:employee.id,
                        first_name:employee.first_name,
                        last_name:employee.last_name,
                        role_id:employee.role_id,
                        id_company:employee.id_company

                    }, `2BD988BF1570DE653C14BA90770F8E4A412DCC0B`);


                return {
                    token: token,
                    id:employee.id,
                    first_name:employee.first_name,
                    last_name:employee.last_name,
                    role_id:employee.role_id,
                    id_company:employee.id_company
                }
            }else{
                throw new ForbiddenError("Username and Password doesn't match!", 403);
            }
        }
    }catch (e) {
        throw e;
    }
};
/*
const logout = async (id) => {
    try {
        let user_exist = await knex('users').where({
            id: id
        }).select('term', 'id').first();

        if(!user_exist.term){
            await knex('users').where('id', '=', user_exist.id).update({
                term: true
            });
        }

    } catch (error) {
        throw error;
    }
}
*/







const daftar = async (data) => {
    try {
        await knex('users').insert({
            ip_address:data.ip_address,
            username:data.username,
            password:'$2y$10$drQ7sDjrdH3dbeRMVYDu3OXXKqwEYC9e2XCJPP7NglcLXSglwx09C',
            email:data.email,
            created_on:moment().format("YYYY-MM-DD HH:mm:ss"),
            active:1,
            first_name:data.first_name,
            last_name:data.last_name,
            phone:data.phone,
            role_id:1

        });
    } catch (error) {
        throw error;
    }
};

module.exports = {
    login,
    daftar,
  //  logout
}
