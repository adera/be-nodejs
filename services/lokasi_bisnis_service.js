const {knex} = require('../config/db');
const moment = require('moment');
const {NotFoundError, ForbiddenError, UniqueError} = require('../exceptions/business_exception');
const {QUERY_STATUS} = require('../constants/index');
const {docNumberGenerator} = require('../utils/util');
const bcrypt = require('bcryptjs');
const { Pool } = require('pg');
const rp = require('request-promise');

const list = async (limit, page, filter) => {
    try {
        const cek = await knex('business_locations');
        return cek;
    } catch (error) {
        throw error;
    }
};

module.exports = {
    list
};
