const {knex} = require('../config/db');
const moment = require('moment');
const {NotFoundError, ForbiddenError, UniqueError} = require('../exceptions/business_exception');
const {QUERY_STATUS} = require('../constants/index');
const {docNumberGenerator} = require('../utils/util');
const bcrypt = require('bcryptjs');
const { Pool } = require('pg');
const rp = require('request-promise');

const list = async (limit, page, filter) => {
    try {
        const cek = await knex('companies')
        .join('companies_types','companies_types.id','=','companies.company_type ')
        .leftJoin('provinces','provinces.id','=','companies.province_id')
        .leftJoin('regencies','regencies.id','=','companies.regency_id')
        .leftJoin('districts','districts.id','=','companies.district_id')
        .leftJoin('villages','villages.id','=','companies.village_id')
        .select('companies.*','companies_types.name as type_company','provinces.name as provinsi','regencies.name as kota','districts.name as kecamatan','villages.name as kelurahan');
        return cek;
    } catch (error) {
        throw error;
    }
};

const simpan = async (user,data) => {
    try {
        const cek = await knex('companies').insert({
            name:data.name,
            address:data.address,
            province_id:data.province_id,
            regency_id:data.regency_id,
            district_id:data.district_id,
            village_id:data.village_id,
            postical_code:data.postical_code,
            phone:data.phone,
            company_type:data.company_type, 
            created_by:user.id,
        }).returning('id');
        console.log('----------')
        console.log(cek.id);
        console.log(cek[0]);
        console.log('----------')
        const cek1 =  await knex('roles').insert({
            name:'Administrator',
            status:1,
            company_id:cek
        }).returning('*');
        await knex('users').insert({
            ip_address:data.ip,
            username:data.username,
            password:data.password,
            email:data.email,
            active:1,
            first_name:data.first_name,
            last_name:data.last_name,
            phone:data.phone1,
            role_id:cek1,
            id_company:cek
        });

        await knex('role_menu').insert({
            id_menu:1,
            id_role:cek1,
            id_company:cek
        });



    } catch (error) {
        throw error;
    }
};


module.exports = {
    list,
    simpan
};
