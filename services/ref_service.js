const {knex,knex2,knex4} = require('../config/db');
const moment = require('moment');
const {NotFoundError, ForbiddenError, UniqueError} = require('../exceptions/business_exception');
const {QUERY_STATUS} = require('../constants/index');
const {docNumberGenerator} = require('../utils/util');
const bcrypt = require('bcryptjs');
const { Pool } = require('pg');
const rp = require('request-promise');

const list_bisnis_type = async (user) => {
    try {
        const cek = await knex('register_business_type');
        return cek;
    } catch (error) {
        throw error;
    }
};

const list_provinsi = async (user) => {
    try {
        const cek = await knex('provinces');
        return cek;
    } catch (error) {
        throw error;
    }
};

const list_bank = async (user) => {
    try {
        const cek = await knex('banks');
        return cek;
    } catch (error) {
        throw error;
    }
};

const list_kota = async (user,id) => {
    try {
        const cek = await knex('regencies').where('province_id ',id);
        return cek;
    } catch (error) {
        throw error;
    }
};

const list_kecamatan = async (user,id) => {
    try {
        const cek = await knex('districts').where('regency_id',id);
        return cek;
    } catch (error) {
        throw error;
    }
};

const list_kelurahan = async (user,id) => {
    try {
        const cek = await knex('villages').where('district_id',id);
        return cek;
    } catch (error) {
        throw error;
    }
};


const list_status = async (user) => {
    try {
        const cek = await knex('register_jawara_status');
        return cek;
    } catch (error) {
        throw error;
    }
};


const list_type_register = async (user) => {
    try {
        const cek = await knex('register_type');
        return cek;
    } catch (error) {
        throw error;
    }
};


const list_type_company = async (user) => {
    try {
        const cek = await knex('companies_types');
        return cek;
    } catch (error) {
        throw error;
    }
};


const daftar = async (data) => {
    try {
        
        const res = await knex('register_masters').insert({
            nik:data.nik, 
            name:data.name, 
            phone:data.phone, 
            email:data.email, 
            reff_code:data.reff_code, 
            company_id:2, 
            campaign_id:2, 
            register_type:0, 
            created_date:moment().format("YYYY-MM-DD"), 
            self_assesment_loan:data.self_assesment_loan
        }).returning('*');

        await knex('register_jawara').insert({
            register_id:res,
            address:data.address,
            rt:0,
            rw:0,
            province_id:data.province_id,
            regency_id:data.regency_id,
            district_id:data.district_id,
            village_id:data.village_id,
            postical_code:data.postical_code,
            store_name:data.store_name,
            store_address:data.store_address,
            store_province_id:data.store_province_id,
            store_regency_id:data.store_regency_id,
            store_district_id:data.store_district_id,
            store_village_id:data.store_village_id,
            file_ktp:data.file_ktp,
            file_kk:data.file_kk,
            file_npwp:data.file_npwp,
            file_store_1:data.file_store_1,
            file_store_2:data.file_store_2,
            file_selfie_1:data.file_selfie_1,
            file_selfie_2:data.file_selfie_2,
            bank_account:data.bank_account,
            bank_account_number:data.bank_account_number,
            bank_account_name:data.bank_account_name,
            self_assesment_loan:data.self_assesment_loan,
            tc_agree:1,
            register_status_id:1

        });
        const cek = await knex('register_masters')
        .join('register_jawara','register_jawara.register_id ','=','register_masters.id')
        .leftJoin('provinces','provinces.id','=','register_jawara.province_id')
        .leftJoin('regencies','regencies.id','=','register_jawara.regency_id')
        .leftJoin('districts','districts.id','=','register_jawara.district_id')
        .leftJoin('villages','villages.id','=','register_jawara.village_id')
        .select('register_masters.*','register_jawara.store_name as business_name','register_jawara.store_address as address','register_jawara.postical_code','provinces.name as provinsi','regencies.name as kota','districts.name as kecamatan','villages.name as kelurahan')
        .where('register_masters.id',res).first();

      


        const res111 = await knex2('contacts').insert({
            business_id:2, 
            type:'customer', 
            name:cek.business_name,    
            contact_id:'CO0001', 
            contact_status:'active', 
            city:cek.kota, 
            state:cek.provinsi, 
            country:'Indonesia', 
            address_line_1:cek.address, 
            mobile:cek.phone, 
            created_by:2, 
            balance:0.0000, 
            total_rp:0, 
            total_rp_used:0, 
            total_rp_expired:0, 
            is_default:0, 
            is_export:0, 
            created_at:moment().format("YYYY-MM-DD"), 
            updated_at:moment().format("YYYY-MM-DD")
        }).returning('*');

     
        await knex2('media').insert({
            business_id:2, 
            file_name:data.file_ktp, 
            description:'File KTP', 
            uploaded_by:2, 
            model_type:'App\Variation', 
            model_id:res111, 
            created_at:moment().format("YYYY-MM-DD"), 
            updated_at:moment().format("YYYY-MM-DD")

        });
        await knex2('media').insert({
            business_id:2, 
            file_name:data.file_kk, 
            description:'File KK', 
            uploaded_by:2, 
            model_type:'App\Variation', 
            model_id:res111, 
            created_at:moment().format("YYYY-MM-DD"), 
            updated_at:moment().format("YYYY-MM-DD")

        });
        await knex2('media').insert({
            business_id:2, 
            file_name:data.file_store_1, 
            description:'File Lokasi Usaha', 
            uploaded_by:2, 
            model_type:'App\Variation', 
            model_id:res111, 
            created_at:moment().format("YYYY-MM-DD"), 
            updated_at:moment().format("YYYY-MM-DD")

        });
        await knex2('media').insert({
            business_id:2, 
            file_name:data.file_selfie_1, 
            description:'File Selfie', 
            uploaded_by:2, 
            model_type:'App\Variation', 
            model_id:res111, 
            created_at:moment().format("YYYY-MM-DD"), 
            updated_at:moment().format("YYYY-MM-DD")

        });

        await knex2('media').insert({
            business_id:2, 
            file_name:data.file_selfie_2, 
            description:'File Selfie dengan KTP', 
            uploaded_by:2, 
            model_type:'App\Variation', 
            model_id:res111, 
            created_at:moment().format("YYYY-MM-DD"), 
            updated_at:moment().format("YYYY-MM-DD")

        });


        

        /** CREATE USER*/
        const res3 = await knex2('users').insert({
            user_type:'user', 
            first_name:cek.business_name, 
            last_name:null, 
            username:cek.email, 
            email:cek.email, 
            password:'$2y$10$tIdN1PRggG6p6uBHoEKqj.5fYR/ABZyiM5Fn0dbW5IlzVgsEjPycO', 
            language:'id',  
            business_id:1, 
            allow_login:1, 
            status:'active',  
            created_at:moment().format("YYYY-MM-DD")
        }).returning('*');

        /** CREATE BISNIS */
        const res1 = await knex2('business').insert({
            name:cek.business_name,
            currency_id:54,
            start_date:moment().format("YYYY-MM-DD"),
            default_profit_percent:25,
            owner_id:res3,
            time_zone:'Asia/Jakarta',
            fy_start_month:1,
            accounting_method:'fifo',
            sell_price_tax:'includes',
            stop_selling_before:0,
            keyboard_shortcuts:'{"pos":{"express_checkout":"shift+e","pay_n_ckeckout":"shift+p","draft":"shift+d","cancel":"shift+c","edit_discount":"shift+i","edit_order_tax":"shift+t","add_payment_row":"shift+r","finalize_payment":"shift+f","recent_product_quantity":"f2","add_new_product":"f4"}}',
            enabled_modules:'["purchases","add_sale","pos_sale","stock_transfers","stock_adjustment","expenses"]',
            ref_no_prefixes:'{"purchase":"PO","stock_transfer":"ST","stock_adjustment":"SA","sell_return":"CN","expense":"EP","contacts":"CO","purchase_payment":"PP","sell_payment":"SP","business_location":"BL"}',
            is_active:1,
            created_at:moment().format("YYYY-MM-DD"),
            updated_at:moment().format("YYYY-MM-DD"),
            weighing_scale_setting:''
        }).returning('*');
        
        /** CREATE BISNIS LOKASI */
       //const getDataExisting11 = await pool2.query(`INSERT INTO business_locations (business_id, location_id, name, landmark, country, state, city, zip_code, invoice_scheme_id, invoice_layout_id, sale_invoice_layout_id, selling_price_group_id, print_receipt_on_invoice, receipt_printer_type, printer_id, mobile, alternate_number, email, website, featured_products, is_active, default_payment_accounts, deleted_at, created_at, updated_at) SELECT `+res1+` business_id,CONCAT('KRI000',max(location_id)+1) location_id,'`+cek.business_name+`' name,'`+cek.address+`' landmark,'Indonesia' country,'`+cek.provinsi+`' state,'`+cek.kota+`' city,'`+cek.postical_code+`' zip_code, 1 'invoice_scheme_id', 1 'invoice_layout_id',1 'sale_invoice_layout_id',NULL selling_price_group_id,1 print_receipt_on_invoice, 'browser' receipt_printer_type, NULL printer_id, '`+cek.phone+`' mobile, '' alternate_number, '`+cek.email+`' email, 'https://myloca.id' website, NULL featured_products, 1 is_active, '{"cash":{"is_enabled":1,"account":null},"card":{"is_enabled":1,"account":null},"cheque":{"is_enabled":1,"account":null},"bank_transfer":{"is_enabled":1,"account":null},"other":{"is_enabled":1,"account":null},"custom_pay_1":{"is_enabled":1,"account":null},"custom_pay_2":{"is_enabled":1,"account":null},"custom_pay_3":{"is_enabled":1,"account":null},"custom_pay_4":{"is_enabled":1,"account":null},"custom_pay_5":{"is_enabled":1,"account":null},"custom_pay_6":{"is_enabled":1,"account":null},"custom_pay_7":{"is_enabled":1,"account":null}}' default_payment_accounts, NULL deleted_at, NOW() created_at , NULL updated_at FROM business_locations`);
     
        const res2 = await knex2('business_locations').insert({
            business_id:res1,
            location_id:'KRI0001',
            name:cek.business_name,
            landmark:cek.address +" "+ cek.kelurahan +" "+ cek.kecamatan,
            country:'INDONESIA',
            state:cek.kota,
            city:cek.kota,
            zip_code:cek.postical_code,
            invoice_scheme_id:1,
            invoice_layout_id:1,
            sale_invoice_layout_id:1,
            print_receipt_on_invoice:1,
            receipt_printer_type:'browser',
            mobile:cek.phone,
            email:cek.email,
            is_active:1,
            default_payment_accounts:'{"cash":{"is_enabled":1,"account":null},"card":{"is_enabled":1,"account":null},"cheque":{"is_enabled":1,"account":null},"bank_transfer":{"is_enabled":1,"account":null},"other":{"is_enabled":1,"account":null},"custom_pay_1":{"is_enabled":1,"account":null},"custom_pay_2":{"is_enabled":1,"account":null},"custom_pay_3":{"is_enabled":1,"account":null},"custom_pay_4":{"is_enabled":1,"account":null},"custom_pay_5":{"is_enabled":1,"account":null},"custom_pay_6":{"is_enabled":1,"account":null},"custom_pay_7":{"is_enabled":1,"account":null}}',
            created_at:moment().format("YYYY-MM-DD"),
            updated_at:moment().format("YYYY-MM-DD")
        }).returning('*');
        
        /**invoice_layouts */
        const res4 = await knex2('invoice_layouts').insert({
            name:'Default',	 
            invoice_no_prefix:'Invoice No.',	 
            invoice_heading:'Invoice',	
            invoice_heading_not_paid:'',	 
            invoice_heading_paid:'',	 
            sub_total_label:'Subtotal',	 
            discount_label:'Discount',	 
            tax_label:'Tax',	 
            total_label:'Total',	 
            total_due_label:'Total Due',	 
            paid_label:'Total Paid',	 
            show_client_id:0,	 
            date_label:'Date',	 
            show_time:1,	
            show_brand:0,	 
            show_sku:1,	 
            show_cat_code:1,	 
            show_expiry:0,	 
            show_lot:0,	 
            show_image:0,	 
            show_sale_description:0,	 
            show_sales_person:0,	 
            table_product_label:'Product',	 
            table_qty_label:'Quantity',	 
            table_unit_price_label:'Unit Price',	 
            table_subtotal_label:'Subtotal',	 
            show_logo:0,	 
            show_business_name:0,	 
            show_location_name:1,	 
            show_landmark:1,	 
            show_city:1,	 
            show_state:1,	 
            show_zip_code:1,	 
            show_country:1,	
            show_mobile_number:1,	 
            show_alternate_number:0,	 
            show_email:0,	 
            show_tax_1:1,	 
            show_tax_2:0,	 
            show_barcode:0,	 
            show_payments:1,	 
            show_customer:1,	 
            customer_label:'Customer',	 
            show_commission_agent:0,	 
            show_reward_point:0,	 
            highlight_color:'#000000',	 
            footer_text:'',	 
            is_default:1,	 
            business_id:res1,	 
            show_qr_code:0,	 
            design:'classic',	 
            show_previous_bal:0,	 
            created_at:moment().format("YYYY-MM-DD"),	 
            updated_at:moment().format("YYYY-MM-DD")
        }).returning('*');

        /**invoice_schemes */
        const res5 = await knex2('invoice_schemes').insert({
            business_id:res1, 
            name:'Default', 
            scheme_type:'blank', 
            prefix:'', 
            start_number:1, 
            invoice_count:0, 
            total_digits:4, 
            is_default:1, 
            created_at:moment().format("YYYY-MM-DD"), 
            updated_at:moment().format("YYYY-MM-DD")
        }).returning('*');

        /** update users */
        await knex2('users').update({
            business_id :res1
        }).where({
            id: res3
        });

        /** create units */
        const res6 = await knex2('units').insert({
            business_id:res1, 
            actual_name:'Pieces', 
            short_name:'Pc(s)', 
            allow_decimal:0, 
            base_unit_id:0, 
            base_unit_multiplier:0, 
            created_by:1, 
            deleted_at:null, 
            created_at:moment().format("YYYY-MM-DD"), 
            updated_at:moment().format("YYYY-MM-DD")
        }).returning('*');

        /**create roles */
        const res7 = await knex2('roles').insert({
            name:'Admin#'+res1, 
            guard_name:'web', 
            business_id:res1, 
            is_default:1, 
            is_service_staff:0, 
            created_at:moment().format("YYYY-MM-DD"), 
            updated_at:moment().format("YYYY-MM-DD")
        }).returning('*');


        const res8 = await knex2('roles').insert({
            name:'Cashier#'+res1, 
            guard_name:'web', 
            business_id:res1, 
            is_default:1, 
            is_service_staff:0, 
            created_at:moment().format("YYYY-MM-DD"), 
            updated_at:moment().format("YYYY-MM-DD")
        }).returning('*');

        /** model_has_roles */
        const res9 = await knex2('model_has_roles').insert({
            role_id:res7, 
            model_type:'App\\User', 
            model_id:res3
        }).returning('*');

        /**notification_templates  */
            let query1=knex2('notification_templates')
            .where({'business_id': 1});
            let res33 = await query1.paginate(1000, 1, true);
            for(let item3 of res33.data){
                await knex2('notification_templates').insert({
                    business_id:res1, 
                    template_for:item3.template_for, 
                    email_body:item3.email_body, 
                    sms_body:item3.sms_body, 
                    whatsapp_text:item3.whatsapp_text, 
                    subject:item3.subject, 
                    cc:item3.cc, 
                    bcc:item3.bcc, 
                    auto_send:item3.auto_send, 
                    auto_send_sms:item3.auto_send_sms, 
                    auto_send_wa_notif:item3.auto_send_wa_notif,
                    created_at:moment().format("YYYY-MM-DD"), 
                    updated_at:moment().format("YYYY-MM-DD")
                });
                
            }  



            /** create permissions */
            const res10 = await knex2('permissions').insert({
                name:'location.'+res2, 
                guard_name:'web', 
                created_at:moment().format("YYYY-MM-DD"), 
                updated_at:moment().format("YYYY-MM-DD")
            }).returning('*');


            /**role_has_permissions */

                await knex2('role_has_permissions').insert({
                    permission_id:25, 
                    role_id:res7
                });
                await knex2('role_has_permissions').insert({
                    permission_id:26, 
                    role_id:res7
                });
                await knex2('role_has_permissions').insert({
                    permission_id:48, 
                    role_id:res7
                });
                await knex2('role_has_permissions').insert({
                    permission_id:49, 
                    role_id:res7
                });
                await knex2('role_has_permissions').insert({
                    permission_id:50, 
                    role_id:res7
                });
                await knex2('role_has_permissions').insert({
                    permission_id:51, 
                    role_id:res7
                });
                await knex2('role_has_permissions').insert({
                    permission_id:80, 
                    role_id:res7
                });


    } catch (error) {
        throw error;
    }
};


const transaksi_isat = async (data) => {
    try {
        
        let cek ='';

        let query1=knex4('transactions')
        .join('transaction_sell_lines','transaction_sell_lines.transaction_id','=','transactions.id')
        .join('business_locations','business_locations.id','=','transactions.location_id')
        .join('business','business.id','=','business_locations.business_id')
        .join('products','products.id','=','transaction_sell_lines.product_id')
        .join('variations','variations.id','=','transaction_sell_lines.variation_id')
        .join('brands','brands.id','=','products.brand_id')
        .join('categories','categories.id','=','products.category_id')
        .where('transactions.type','sell')
        .select('brands.id','brands.name')
        .groupBy('brands.id','brands.name');
        let res3 = await query1.paginate(1000, 1, true);
        let brands = [];    
        let dt = [];
        for(let item3 of res3.data){
            brands.push({
                brandsid: item3.id,
                brands: item3.name  
            });
            if(data.grup=='Bisnis'){
            cek = knex4('transactions')
            .join('transaction_sell_lines','transaction_sell_lines.transaction_id','=','transactions.id')
            .join('business_locations','business_locations.id','=','transactions.location_id')
            .join('business','business.id','=','business_locations.business_id')
            .join('products','products.id','=','transaction_sell_lines.product_id')
            .join('variations','variations.id','=','transaction_sell_lines.variation_id')
            .join('brands','brands.id','=','products.brand_id')
            .join('categories','categories.id','=','products.category_id')
            .where('transactions.type','sell')
            .where('brands.id',item3.id)
            .select(knex.raw(`sum(transaction_sell_lines.quantity) as quantity`))
            .select('business.name','brands.name as brand')
            .groupBy('business.name','brands.name');
            }else if(data.grup=='Provinsi'){
            cek = knex4('transactions')
                .join('transaction_sell_lines','transaction_sell_lines.transaction_id','=','transactions.id')
                .join('business_locations','business_locations.id','=','transactions.location_id')
                .join('business','business.id','=','business_locations.business_id')
                
                .join('products','products.id','=','transaction_sell_lines.product_id')
                .join('variations','variations.id','=','transaction_sell_lines.variation_id')
                .join('brands','brands.id','=','products.brand_id')
                .join('categories','categories.id','=','products.category_id')
                
                .where('transactions.type','sell')
                .where('brands.id',item3.id)
                .select(knex.raw(`sum(transaction_sell_lines.quantity) as quantity`))
                .select('business_locations.state as name','brands.name as brand')
                .groupBy('business_locations.state','brands.name'); 
            }else{
            cek = knex4('transactions')
                .join('transaction_sell_lines','transaction_sell_lines.transaction_id','=','transactions.id')
                .join('business_locations','business_locations.id','=','transactions.location_id')
                .join('business','business.id','=','business_locations.business_id')
                
                .join('products','products.id','=','transaction_sell_lines.product_id')
                .join('variations','variations.id','=','transaction_sell_lines.variation_id')
                .join('brands','brands.id','=','products.brand_id')
                .join('categories','categories.id','=','products.category_id')
                
                .where('transactions.type','sell')
                .where('brands.id',item3.id)
                .select(knex.raw(`sum(transaction_sell_lines.quantity) as quantity`))
                .select('business_locations.city as name','brands.name as brand')
                .groupBy('business_locations.city','brands.name');  
            }    


            let res4 = await cek.paginate(9000, 1, true);
            for(let item4 of res4.data){
                dt.push({
                    brands:item4.brand,
                    name: item4.name,
                    qty: item4.quantity  
                });

            }
            
            
            
            
        }    
        return {
            brands:brands,
            data:dt
        }

       
    } catch (error) {
        throw error;
    }
};

module.exports = {
    list_bisnis_type,
    list_provinsi,
    list_kota,
    list_kecamatan,
    list_kelurahan,
    list_bank,
    list_status,
    list_type_register,
    list_type_company,
    daftar,
    transaksi_isat
};
