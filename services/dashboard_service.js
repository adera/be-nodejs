const {knex,knex1} = require('../config/db');
const moment = require('moment');
const {NotFoundError, ForbiddenError, UniqueError} = require('../exceptions/business_exception');
const {QUERY_STATUS} = require('../constants/index');
const {docNumberGenerator} = require('../utils/util');
const bcrypt = require('bcryptjs');
const { Pool } = require('pg');
const rp = require('request-promise');
const { raw } = require('body-parser');

const list = async (user) => {
    try {
        console.log(user);
        const cek = await knex('register_masters')
        .select(knex.raw(`count(*) as total`))
        .where('company_id',user.id_company);
        return cek;
    } catch (error) {
        throw error;
    }
};

const list1 = async (user) => {
    try {
        console.log(user);
       const x =await knex('register_business').select('register_id as id');
       console.log(x); 
       const cek = await knex('register_masters')
       .whereNotExists(function() {
        this.select('*').from('register_business').whereRaw('register_masters.id = register_business.register_id');
        })
        .select(knex.raw(`count(*) as total`))
        .where('company_id',2);
        
        return cek;
    } catch (error) {
        throw error;
    }
    /*
    try {
        console.log(user);
        const cek = await knex('register_masters')
        .where('campaign_id ',1);
        return cek;
    } catch (error) {
        throw error;
    }
    */
};




const list2 = async (user) => {
    try {
        console.log(user);
        const cek = await knex1('transactions')
        .select(knex1.raw(`count(*) as total`))
        .whereRaw(`EXTRACT(MONTH FROM transaction_date) = ?`, [moment().format("MM")])
        .whereRaw(`EXTRACT(YEAR FROM transaction_date) = ?`, [moment().format("YYYY")]);
        return cek;

    } catch (error) {
        throw error;
    }
};


const list3 = async (user) => {
    try {
        console.log(user);
        const cek = await knex1('transactions')
        .select(knex1.raw(`sum(shipping_charges) as total`))
        .where('shipping_charges','<>',0)
        .whereRaw(`EXTRACT(MONTH FROM transaction_date) = ?`, [moment().format("MM")])
        .whereRaw(`EXTRACT(YEAR FROM transaction_date) = ?`, [moment().format("YYYY")]);
        return cek;
    } catch (error) {
        throw error;
    }
};

const list_bisnis = async (user) => {
    try {
        console.log(user);
        const cek = await knex1('business')
        .select(knex1.raw(`count(*) as total`))
        return cek;
    } catch (error) {
        throw error;
    }
};

const list_bisnis_lokasi = async (user) => {
    try {
        console.log(user);
        const cek = await knex1('business_locations')
        .select(knex1.raw(`count(*) as total`))
        return cek;
    } catch (error) {
        throw error;
    }
};


module.exports = {
    list,
    list1,
    list2,
    list3,
    list_bisnis,
    list_bisnis_lokasi

};
