const {knex} = require('../config/db');
const moment = require('moment');
const {NotFoundError, ForbiddenError, UniqueError} = require('../exceptions/business_exception');
const {QUERY_STATUS} = require('../constants/index');
const {docNumberGenerator} = require('../utils/util');
const bcrypt = require('bcryptjs');
const { Pool } = require('pg');
const rp = require('request-promise');

const list = async (limit, page, filter) => {
    try {
        const cek = await knex('roles');
        return cek;
    } catch (error) {
        throw error;
    }
};

const byid = async (limit, page, id) => {
    try {
        const cek = await knex('roles').where('id',id);
        return cek;
    } catch (error) {
        throw error;
    }
};

const hapus = async (limit, page, id) => {
    try {
        const cek = await knex('roles').update({
            status:2  
        }).where({
            id: id
        });
        return cek;
    } catch (error) {
        throw error;
    }
};

const daftar = async (data) => {
    try {
        await knex('roles').insert({
            name:data.name,
            status:1
    });
    } catch (error) {
        throw error;
    }
};

const edit = async (data) => {
    try {
        const cek = await knex('roles').update({
            name:data.name  
        }).where({
            id: id
        });
        return cek;
    } catch (error) {
        throw error;
    }
};



module.exports = {
    list,
    byid,
    edit,
    hapus,
    daftar
};
