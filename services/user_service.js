const {knex} = require('../config/db');
const moment = require('moment');
const {NotFoundError, ForbiddenError, UniqueError} = require('../exceptions/business_exception');
const {QUERY_STATUS} = require('../constants/index');
const {docNumberGenerator} = require('../utils/util');
const bcrypt = require('bcryptjs');
const { Pool } = require('pg');
const rp = require('request-promise');

const list = async (limit, page, filter) => {
    try {
        const cek = await knex('users')
        .join('companies','companies.id','=','users.id_company')
        .join('roles','roles.id','=','users.role_id')
        .select('users.*','companies.name as company','roles.name as role');
        return cek;
    } catch (error) {
        throw error;
    }
};

const detail = async (user,id) => {
    try {
        const cek = await knex('users')
        .join('companies','companies.id','=','users.id_company')
        .join('roles','roles.id','=','users.role_id')
        .select('users.*','companies.name as company','roles.name as role')
        .where('users.id',id);
        
        return cek;
    } catch (error) {
        throw error;
    }
};


const update = async (data) => {
    try {
        const cek = await knex('users').update({
            username:data.username,
            email:data.email,
            first_name:data.first_name,
            last_name:data.last_name,
            phone:data.phone,
            referall_code:data.referall_code
        }).where({
            id: data.id
        });
        return cek;
    } catch (error) {
        throw error;
    }
};


const non_aktif = async (user,id) => {
    try {
        const cek = await knex('users').update({
            active:0,
        }).where({
            id:id
        });
        return cek;
    } catch (error) {
        throw error;
    }
};

const aktif = async (user,id) => {
    try {
        const cek = await knex('users').update({
            active:1,
        }).where({
            id:id
        });
        return cek;
    } catch (error) {
        throw error;
    }
};

const daftar = async (user,data) => {
    try {
        console.log(user);
        const cek = await knex('users').insert({
            ip_address:data.ip,
            username:data.username,
            password:data.password,
            email:data.email,
            active:1,
            first_name:data.first_name,
            last_name:data.last_name,
            phone:data.phone,
            role_id:0,
            id_company:user.id_company,
            referall_code:data.referall_code
        });
        
        return cek;
    } catch (error) {
        throw error;
    }
};

module.exports = {
    list,
    detail,
    update,
    non_aktif,
    aktif,
    daftar
};
