const route = require('express').Router();
const {login, logout,daftar} = require('../services/auth_service');
const {sendResponseSuccess} = require('../utils/util')
const {checkToken} = require('../utils/middleware');

route.post('/login', async (req, res, next) => {
    try{
        console.log(req.body);
        return sendResponseSuccess(res, await login(req.body.username, req.body.password), "Login Success", 200);
    }catch (e) {
        return next(e);
    }
});

route.post('/logout', checkToken, async (req, res, next) => {
    try{
        console.log(req.body);
        return sendResponseSuccess(res, await logout(req.user.user_id), "Logout Sucess", 200)
    }catch(e){
        return next(e);
    }
});

route.post('/', async (req, res, next) => {
    try {
        
		//validateRequest(req.body, schema);
        return sendResponseSuccess(res, await daftar(req.body))
    } catch (error) {
        next(error);
    }
});

module.exports = route;