const route = require('express').Router();
const {sendResponseSuccess, validateRequest} = require('../utils/util');
const {list,list1,list_detail,daftar,daftar1,list_mitra,list_mitra_detail,list_japang,list_mitra_detail1,update_status} = require('../services/register_service');

route.get('/register-loca', async (req, res, next) => {
    try {     
		return sendResponseSuccess(res, await list(req.user))
    } catch (error) {
        next(error);
    }
});

route.get('/register-loca/:id', async (req, res, next) => {
    try {     
		return sendResponseSuccess(res, await list_detail(req.user,req.params.id))
    } catch (error) {
        next(error);
    }
});

route.get('/register-jawara', async (req, res, next) => {
    try {     
		return sendResponseSuccess(res, await list1(req.user))
    } catch (error) {
        next(error);
    }
});

route.post('/', async (req, res, next) => {
    try {
        
		//validateRequest(req.body, schema);
        return sendResponseSuccess(res, await daftar(req.body))
    } catch (error) {
        next(error);
    }
});


route.post('/jawara', async (req, res, next) => {
    try {
        
		//validateRequest(req.body, schema);
        return sendResponseSuccess(res, await daftar1(req.body))
    } catch (error) {
        next(error);
    }
});

route.get('/mitra-loca/:id', async (req, res, next) => {
    try {     
		return sendResponseSuccess(res, await list_mitra(req.user,req.params.id))
    } catch (error) {
        next(error);
    }
});

route.get('/detail-mitra-loca/:id', async (req, res, next) => {
    try {     
		return sendResponseSuccess(res, await list_mitra_detail(req.user,req.params.id))
    } catch (error) {
        next(error);
    }
});


route.get('/mitra-japang/:id', async (req, res, next) => {
    try {     
		return sendResponseSuccess(res, await list_japang(req.user,req.params.id))
    } catch (error) {
        next(error);
    }
});


route.get('/detail-mitra-japang/:id', async (req, res, next) => {
    try {     
		return sendResponseSuccess(res, await list_mitra_detail1(req.user,req.params.id))
    } catch (error) {
        next(error);
    }
});


route.post('/status-jawara', async (req, res, next) => {
    try {
        
		//validateRequest(req.body, schema);
        return sendResponseSuccess(res, await update_status(req.user,req.body))
    } catch (error) {
        next(error);
    }
});

module.exports = route;