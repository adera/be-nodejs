const route = require('express').Router();
const {sendResponseSuccess, validateRequest} = require('../utils/util');
const {list} = require('../services/transaksi_service');



route.get('/', async (req, res, next) => {
    try {
        return sendResponseSuccess(res, await list(req.query.limit, req.query.page))
    } catch (error) {
        next(error);
    }
});


module.exports = route;