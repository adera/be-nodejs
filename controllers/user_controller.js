const route = require('express').Router();
const {sendResponseSuccess, validateRequest} = require('../utils/util');
const {list,detail,update,non_aktif,aktif,daftar} = require('../services/user_service');



route.get('/', async (req, res, next) => {
    try {
        return sendResponseSuccess(res, await list(req.query.limit, req.query.page))
    } catch (error) {
        next(error);
    }
});


route.get('/:id', async (req, res, next) => {
    try {
        return sendResponseSuccess(res, await detail(req.user, req.params.id))
    } catch (error) {
        next(error);
    }
});

route.put('/', async (req, res, next) => {
    try {
        
		return sendResponseSuccess(res, await update(req.body))
    } catch (error) {
        next(error);
    }
});

route.get('/non-aktif/:id', async (req, res, next) => {
    try {
        return sendResponseSuccess(res, await non_aktif(req.user,req.params.id))
    } catch (error) {
        next(error);
    }
});

route.get('/aktif/:id', async (req, res, next) => {
    try {
        return sendResponseSuccess(res, await aktif(req.user,req.params.id))
    } catch (error) {
        next(error);
    }
});

route.post('/', async (req, res, next) => {
    try {
        
		return sendResponseSuccess(res, await daftar(req.user,req.body))
    } catch (error) {
        next(error);
    }
});

module.exports = route;