const route = require('express').Router();
const {sendResponseSuccess, validateRequest} = require('../utils/util');
const {list,simpan} = require('../services/company_service');



route.get('/', async (req, res, next) => {
    try {
        return sendResponseSuccess(res, await list(req.query.limit, req.query.page))
    } catch (error) {
        next(error);
    }
});


route.post('/', async (req, res, next) => {
    try {
        return sendResponseSuccess(res, await simpan(req.user,req.body))
    } catch (error) {
        next(error);
    }
});


module.exports = route;