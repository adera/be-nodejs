const route = require('express').Router();
const {sendResponseSuccess, validateRequest} = require('../utils/util');
const {list,byid,hapus,daftar,edit} = require('../services/role_service');

route.post('/', async (req, res, next) => {
    try {
        
		return sendResponseSuccess(res, await daftar(req.body))
    } catch (error) {
        next(error);
    }
});

route.put('/', async (req, res, next) => {
    try {
        
		return sendResponseSuccess(res, await edit(req.body))
    } catch (error) {
        next(error);
    }
});

route.get('/', async (req, res, next) => {
    try {
        return sendResponseSuccess(res, await list(req.query.limit, req.query.page))
    } catch (error) {
        next(error);
    }
});

route.get('/:id', async (req, res, next) => {
    try {

        return sendResponseSuccess(res, await byid(req.query.limit, req.query.page,req.params.id))
    } catch (error) {
        next(error);
    }
});

route.get('/hapus/:id', async (req, res, next) => {
    try {

        return sendResponseSuccess(res, await hapus(req.query.limit, req.query.page,req.params.id))
    } catch (error) {
        next(error);
    }
});


module.exports = route;