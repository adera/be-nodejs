const route = require('express').Router();
const {sendResponseSuccess, validateRequest} = require('../utils/util');
const {list} = require('../services/role_menu_service');

route.get('/', async (req, res, next) => {
    try {     
		return sendResponseSuccess(res, await list(req.user))
    } catch (error) {
        next(error);
    }
});



module.exports = route;