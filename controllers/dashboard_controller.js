const route = require('express').Router();
const {sendResponseSuccess, validateRequest} = require('../utils/util');
const {list,list1,list2,list3,list_bisnis,list_bisnis_lokasi} = require('../services/dashboard_service');

route.get('/dashboard-loca-register', async (req, res, next) => {
    try {     
		return sendResponseSuccess(res, await list(req.user))
    } catch (error) {
        next(error);
    }
});


route.get('/dashboard-loca-aktif', async (req, res, next) => {
    try {     
		return sendResponseSuccess(res, await list1(req.user))
    } catch (error) {
        next(error);
    }
});



route.get('/dashboard-loca-transaksi', async (req, res, next) => {
    try {     
		return sendResponseSuccess(res, await list2(req.user))
    } catch (error) {
        next(error);
    }
});

route.get('/dashboard-loca-layanan', async (req, res, next) => {
    try {     
		return sendResponseSuccess(res, await list3(req.user))
    } catch (error) {
        next(error);
    }
});


route.get('/dashboard-bisnis', async (req, res, next) => {
  try {     
  return sendResponseSuccess(res, await list_bisnis(req.user))
  } catch (error) {
      next(error);
  }
});

route.get('/dashboard-bisnis-lokasi', async (req, res, next) => {
  try {     
  return sendResponseSuccess(res, await list_bisnis_lokasi(req.user))
  } catch (error) {
      next(error);
  }
});

module.exports = route;