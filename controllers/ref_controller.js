const route = require('express').Router();
const {sendResponseSuccess, validateRequest} = require('../utils/util');
const {list_bisnis_type,transaksi_isat,list_type_company,list_provinsi,list_kota,list_kecamatan,list_kelurahan,list_bank,list_status,list_type_register,daftar} = require('../services/ref_service');

route.get('/bisnis-type', async (req, res, next) => {
    try {     
		return sendResponseSuccess(res, await list_bisnis_type(req.user))
    } catch (error) {
        next(error);
    }
});

route.get('/provinsi', async (req, res, next) => {
    try {     
		return sendResponseSuccess(res, await list_provinsi(req.user))
    } catch (error) {
        next(error);
    }
});

route.get('/bank', async (req, res, next) => {
  try {     
  return sendResponseSuccess(res, await list_bank(req.user))
  } catch (error) {
      next(error);
  }
});

route.get('/type-register', async (req, res, next) => {
  try {     
  return sendResponseSuccess(res, await list_type_register(req.user))
  } catch (error) {
      next(error);
  }
});

route.get('/status-japang', async (req, res, next) => {
  try {     
  return sendResponseSuccess(res, await list_status(req.user))
  } catch (error) {
      next(error);
  }
});

route.get('/kota/:id', async (req, res, next) => {
    try {     
		return sendResponseSuccess(res, await list_kota(req.user,req.params.id))
    } catch (error) {
        next(error);
    }
});

route.get('/kecamatan/:id', async (req, res, next) => {
    try {     
		return sendResponseSuccess(res, await list_kecamatan(req.user,req.params.id))
    } catch (error) {
        next(error);
    }
});

route.get('/kelurahan/:id', async (req, res, next) => {
    try {     
		return sendResponseSuccess(res, await list_kelurahan(req.user,req.params.id))
    } catch (error) {
        next(error);
    }
});

route.get('/type-company', async (req, res, next) => {
  try {     
  return sendResponseSuccess(res, await list_type_company(req.user))
  } catch (error) {
      next(error);
  }
});

route.post('/register-jawara', async (req, res, next) => {
  try {
      
  //validateRequest(req.body, schema);
      return sendResponseSuccess(res, await daftar(req.body))
  } catch (error) {
      next(error);
  }
});


route.post('/transaksi-isat', async (req, res, next) => {
  try {
      
  //validateRequest(req.body, schema);
      return sendResponseSuccess(res, await transaksi_isat(req.body))
  } catch (error) {
      next(error);
  }
});


module.exports = route;